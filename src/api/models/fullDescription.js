var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var FullDescriptionAtomized = new Schema ({
	measurementOrFact : MeasurementOrFact,
	ancillaryData : AncillaryData
},{ versionKey: false });

var FullDescription = new AbstractElementSchema();
FullDescription.add({
	fullDescriptionAtomized : [FullDescriptionAtomized],
	fullDescriptionUnstructured : String
});


var FullDescriptionVersion = new AbstractElementVersionSchema();
FullDescriptionVersion.add({
	fullDescription : FullDescription
});
module.exports = mongoose.model('FullDescriptionVersion', FullDescriptionVersion, 'FullDescriptionVersion');

