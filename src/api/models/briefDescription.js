var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema




var BriefDescriptionVersion = new AbstractElementVersionSchema();
BriefDescriptionVersion.add({
	briefDescription : String
});
module.exports = mongoose.model('BriefDescriptionVersion', BriefDescriptionVersion, 'BriefDescriptionVersion');

