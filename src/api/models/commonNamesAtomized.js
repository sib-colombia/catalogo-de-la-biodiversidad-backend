var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema


//temporalCoverage : { startDate : { type: Date, required: true, default: Date.now }, endDate : { type: Date, required: true, default: Date.now } },
var Distribution = new Schema({
	distributionScope : { type : {type: String}, ancillaryData: [AncillaryData] },
	temporalCoverage : { startDate : { type: Date, required: true, default: Date.now }, endDate : { type: Date, required: true, default: Date.now } },
	distributionAtomizedBranch: [String],
	distributionUnstructured : String
});


var CommonNamesAtomized = new AbstractElementSchema();
CommonNamesAtomized.add({
	name : String,
	language : String,
	synonymStatus : String,
	usedIn : Distribution,
	usedBy : String
});


var CommonNamesAtomizedVersion = new AbstractElementVersionSchema();
CommonNamesAtomizedVersion.add({
	commonNamesAtomized : [CommonNamesAtomized]
});
module.exports = mongoose.model('CommonNamesAtomizedVersion', CommonNamesAtomizedVersion, 'CommonNamesAtomizedVersion');


