var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var Territory = new AbstractElementSchema();
Territory.add({
	territoryAtomized: {
		extentOfOccurrence : String,
		areaOfOccupancy : String
	},
	territoryUnstructured : String
});


var TerritoryVersion = new AbstractElementVersionSchema();
TerritoryVersion.add({
	territory : Territory
});
module.exports = mongoose.model('TerritoryVersion', TerritoryVersion, 'TerritoryVersion');

