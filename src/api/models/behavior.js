var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var BehaviorAtomized = new Schema ({
	measurementOrFact : MeasurementOrFact,
	ancillaryData : AncillaryData
},{ versionKey: false });

var Behavior = new AbstractElementSchema();
Behavior.add({
	behaviorAtomized : [BehaviorAtomized],
	behaviorUnstructured : String
});


var BehaviorVersion = new AbstractElementVersionSchema();
BehaviorVersion.add({
	behavior : Behavior
});
module.exports = mongoose.model('BehaviorVersion', BehaviorVersion, 'BehaviorVersion');


