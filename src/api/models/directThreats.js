var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var DirectThreatsAtomized = new AbstractElementSchema();
DirectThreatsAtomized.add({
	measurementOrFact : MeasurementOrFact
});

var DirectThreats = new AbstractElementSchema();
DirectThreats.add({
	directThreatsAtomized : [DirectThreatsAtomized],
	directThreatsUnstructured : String
});


var DirectThreatsVersion = new AbstractElementVersionSchema();
DirectThreatsVersion.add({
	directThreats : DirectThreats
});
module.exports = mongoose.model('DirectThreatsVersion', DirectThreatsVersion, 'DirectThreatsVersion');


