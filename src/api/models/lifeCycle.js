var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var LifeCycleAtomized = new Schema ({
	measurementOrFact : MeasurementOrFact,
	ancillaryData : AncillaryData
},{ versionKey: false });

var LifeCycle = new AbstractElementSchema();
LifeCycle.add({
	lifeCycleAtomized : [LifeCycleAtomized],
	lifeCycleUnstructured : String
});


var LifeCycleVersion = new AbstractElementVersionSchema();
LifeCycleVersion.add({
	lifeCycle : LifeCycle
});
module.exports = mongoose.model('LifeCycleVersion', LifeCycleVersion, 'LifeCycleVersion');

