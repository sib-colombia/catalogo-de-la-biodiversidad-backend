var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var Hierarchy = new AbstractElementSchema();
Hierarchy.add({
	classification: String,
	recommended: Number,
	kingdom : String,
	phylum : String,
	classHierarchy: String,
	order : String,
	family : String,
	genus : String,
	subgenus : String,
	taxonRank : String,
	specificEpithet : String,
	infraspecificEpithet : String,
	higherClassification : String,
	parentTaxon : String
});


var HierarchyVersion = new AbstractElementVersionSchema();
HierarchyVersion.add({
	hierarchy : [Hierarchy]
});
module.exports = mongoose.model('HierarchyVersion', HierarchyVersion, 'HierarchyVersion');

