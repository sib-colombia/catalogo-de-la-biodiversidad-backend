var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var BaseElements = new AbstractElementSchema();
BaseElements.add({
	taxonRecordID : String, //ObjectID?
	taxonConceptID : String,
	globalUniqueIdentifier : String,
	abstractBaseElement : String
});


var BaseElementsVersion = new AbstractElementVersionSchema();
BaseElementsVersion.add({
	baseElements : BaseElements
});
module.exports = mongoose.model('BaseElementsVersion', BaseElementsVersion, 'BaseElementsVersion');
