var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var EcologicalSignificanceAtomized = new AbstractElementSchema();
EcologicalSignificanceAtomized.add({
	measurementOrFact : MeasurementOrFact
});

var EcologicalSignificance = new AbstractElementSchema();
EcologicalSignificance.add({
	ecologicalSignificanceAtomized : [EcologicalSignificanceAtomized],
	ecologicalSignificanceUnstructured : String,
});


var EcologicalSignificanceVersion = new AbstractElementVersionSchema();
EcologicalSignificanceVersion.add({
	ecologicalSignificance : EcologicalSignificance
});
module.exports = mongoose.model('EcologicalSignificanceVersion', EcologicalSignificanceVersion, 'EcologicalSignificanceVersion');

