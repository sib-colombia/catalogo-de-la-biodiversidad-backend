var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var PopulationBiologyAtomized = new AbstractElementSchema();
PopulationBiologyAtomized.add({
	region : {
		measurementValue : String
	},
	abundanceData : {
		measurementValue : String
	},
	densityData : {
		measurementValue : String
	},
	patternDistribution : {
		measurementValue : String
	},
	size : {
		measurementValue : String
	},
	sexRatio : {
		measurementValue : String
	},
	fecundity : {
		measurementValue : String
	},
	mortalityRate : {
		measurementValue : String
	},
	birthRate : {
		measurementValue : String
	},
	numberIndividualsPerObservation : {
		measurementValue : String
	},
	averageDensity : {
		measurementValue : String
	},
	populationTrend : {
		measurementValue : String
	},
	recruitment : {
		measurementValue : String
	},
	populationGrowthRate : {
		measurementValue : String
	},
	emigration : {
		measurementValue : String
	},
	immigration : {
		measurementValue : String
	},
	descriptionLifeStages : {
		measurementValue : String
	},
	proportionIndividualsPerStageLife : {
		measurementValue : String
	},
	carryingCapacity : {
		measurementValue : String
	}
});

var PopulationBiology = new AbstractElementSchema();
PopulationBiology.add({
	populationBiologyAtomized: [PopulationBiologyAtomized],
	populationBiologyUnstructured : String
});

var PopulationBiologyVersion = new AbstractElementVersionSchema();
PopulationBiologyVersion.add({
	populationBiology : PopulationBiology
});
module.exports = mongoose.model('PopulationBiologyVersion', PopulationBiologyVersion, 'PopulationBiologyVersion');


