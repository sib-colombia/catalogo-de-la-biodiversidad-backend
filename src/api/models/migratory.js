var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var MigratoryAtomized = new AbstractElementSchema();
MigratoryAtomized.add({
	causes : String,
	patterns : String,
	routes : String,
	season : String
});

var Migratory = new AbstractElementSchema();
Migratory.add({
	migratoryAtomized : [MigratoryAtomized],
	migratoryUnstructured : String,
	additionalInformation : String,
	dataObject : String,
});


var MigratoryVersion = new AbstractElementVersionSchema();
MigratoryVersion.add({
	migratory : Migratory
});
module.exports = mongoose.model('MigratoryVersion', MigratoryVersion, 'MigratoryVersion');

