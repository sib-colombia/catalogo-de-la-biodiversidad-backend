var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var DistributionScope = new AbstractElementSchema();
DistributionScope.add({
	type : String,
});

var distributionAtomized = new AbstractElementSchema();
distributionAtomized.add({
	country : String,
	county : String,
	municipality: String,
	locality : String,
	stateProvince: String
});

var Distribution = new AbstractElementSchema();
Distribution.add({
	distributionScope: DistributionScope,
	temporalCoverage:{
		startDate : {type: Date, default: Date.now()},
		endDate: {type: Date, default: Date.now()}
	},
	distributionAtomized : [distributionAtomized],
	distributionUnstructured : String
});


var DistributionVersion = new AbstractElementVersionSchema();
DistributionVersion.add({
	distribution : [Distribution]
});
module.exports = mongoose.model('DistributionVersion', DistributionVersion, 'DistributionVersion');


