var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var EnvironmentalEnvelopeAtomized = new AbstractElementSchema();
EnvironmentalEnvelopeAtomized.add({
	measurementOrFact : MeasurementOrFact
});

var EnvironmentalEnvelope = new AbstractElementSchema();
EnvironmentalEnvelope.add({
	environmentalEnvelopeAtomized : [EnvironmentalEnvelopeAtomized],
	environmentalEnvelopeUnstructured : String,
});

var EnvironmentalEnvelopeVersion = new AbstractElementVersionSchema();
EnvironmentalEnvelopeVersion.add({
	environmentalEnvelope : EnvironmentalEnvelope
});
module.exports = mongoose.model('EnvironmentalEnvelopeVersion', EnvironmentalEnvelopeVersion, 'EnvironmentalEnvelopeVersion');


