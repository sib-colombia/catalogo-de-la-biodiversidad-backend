var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;

var AncillaryDataVersion = new AbstractElementVersionSchema();
AncillaryDataVersion.add({
	ancillaryData : [AncillaryData]
});



module.exports = mongoose.model( 'AncillaryDataVersion', AncillaryDataVersion, 'AncillaryDataVersion');
