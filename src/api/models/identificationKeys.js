var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var IdentificationKeys = new AbstractElementSchema();
IdentificationKeys.add({
	keys : String
});


var IdentificationKeysVersion = new AbstractElementVersionSchema();
IdentificationKeysVersion.add({
	identificationKeys : IdentificationKeys
});
module.exports = mongoose.model('IdentificationKeysVersion', IdentificationKeysVersion, 'IdentificationKeysVersion');

