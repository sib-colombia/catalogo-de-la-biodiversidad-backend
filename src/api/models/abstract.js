var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var RecordVersion = require('mongoose').model('RecordVersion').schema;


var AbstractVersion = new AbstractElementVersionSchema();
AbstractVersion.add({
	abstract : String
});


module.exports = mongoose.model('AbstractVersion', AbstractVersion, 'AbstractVersion');
