var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var threatStatusAtomized = new AbstractElementSchema();
threatStatusAtomized.add({
	threatCategory: {
		measurementID : String,
		measurementType : String,
		measurementAccuracy : String,
		measurementUnit : String,
		measurementDeterminedDate : Date,
		measurementDeterminedBy : [String],
		measurementMethod: String,
		measurementRemarks : String,
		relatedTo: String,
		measurementValue: String
	},
	authority: [String],
	appliesTo: {
		country : String,
		stateProvince: String,
		county : String,
		municipality: String,
		locality: String
	},
	apendiceCITES: [String]
});

var ThreatStatus = new AbstractElementSchema();
ThreatStatus.add({
	threatStatusAtomized : threatStatusAtomized,
	threatStatusUnstructured : String
});

var ThreatStatusVersion = new AbstractElementVersionSchema();
ThreatStatusVersion.add({
	threatStatus : [ThreatStatus]
});
module.exports = mongoose.model('ThreatStatusVersion', ThreatStatusVersion, 'ThreatStatusVersion');

