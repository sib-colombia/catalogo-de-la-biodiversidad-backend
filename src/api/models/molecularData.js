var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
//var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var InnerMeasurementOrFact = new Schema({
	measurementID : String,
	measurementType : String,
	measurementValue : String,
	measurementAccuracy : String,
	measurementUnit : String,
	measurementDeterminedDate : String,
	measurementDeterminedBy: [String],
	measurementMethod : String,
	measurementRemarks : String,
	relatedTo : String
},{ collection : 'measurementOrFact', strict: false });

var MeasurementOrFact = new Schema({
	measurementOrFact : InnerMeasurementOrFact,
	relatedTo : String
},{ collection : 'measurementOrFact', strict: false });

var MolecularDataAtomized = new AbstractElementSchema();
MolecularDataAtomized.add({
	measurementOrFact : MeasurementOrFact,
	relatedTo : String
});

var MolecularData = new AbstractElementSchema();
MolecularData.add({
	molecularDataAtomized : [MolecularDataAtomized],
	molecularDataUnstructured : String
});


var MolecularDataVersion = new AbstractElementVersionSchema();
MolecularDataVersion.add({
	molecularData : MolecularData
});
module.exports = mongoose.model('MolecularDataVersion', MolecularDataVersion, 'MolecularDataVersion');

