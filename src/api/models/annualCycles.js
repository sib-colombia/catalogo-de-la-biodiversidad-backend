var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;

var AnnualCycleAtomized = new Schema ({
	event : String,
	startTimeInterval : String,
	endTimeInterval : String,
	ancillaryData : AncillaryData
},{ strict: false, versionKey: false });


var AnnualCycles = new AbstractElementVersionSchema();
AnnualCycles.add({
	annualCycleAtomized : [AnnualCycleAtomized],
	annualCycleUnstructured : String
});

var AnnualCyclesVersion = new AbstractElementVersionSchema();
AnnualCyclesVersion.add({
	annualCycles : AnnualCycles
});


module.exports = mongoose.model('AnnualCyclesVersion', AnnualCyclesVersion, 'AnnualCyclesVersion');



