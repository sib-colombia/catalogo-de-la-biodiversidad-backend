var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var InteractionSpeciesType = new Schema({
	measurementOrFact : MeasurementOrFact,
	ancillaryData : AncillaryData
},{ versionKey: false });

var InteractionsAtomized = new Schema ({
	interactionSpecies : String, 
	interactionSpeciesType : [InteractionSpeciesType],
	ancillaryData : [AncillaryData]
},{ versionKey: false });

var Interactions = new AbstractElementSchema();
Interactions.add({
	interactionsAtomized : [InteractionsAtomized],
	interactionsUnstructured : String
});


var InteractionsVersion = new AbstractElementVersionSchema();
InteractionsVersion.add({
	interactions: Interactions
});
module.exports = mongoose.model('InteractionsVersion', InteractionsVersion, 'InteractionsVersion');

