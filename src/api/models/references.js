var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var Reference = require('mongoose').model('Reference').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema

var ReferencesVersion = new AbstractElementVersionSchema();
ReferencesVersion.add({
	references : [Reference]
});
module.exports = mongoose.model('ReferencesVersion', ReferencesVersion, 'ReferencesVersion');

