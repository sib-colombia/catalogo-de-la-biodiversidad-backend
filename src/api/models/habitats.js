var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var HabitatAtomized = new AbstractElementSchema();
HabitatAtomized.add({
	measurementOrFact : MeasurementOrFact,
	relatedTo : String
});

var Habitats = new AbstractElementSchema();
Habitats.add({
	habitatAtomized : [HabitatAtomized],
	habitatUnstructured : String
});


var HabitatsVersion = new AbstractElementVersionSchema();
HabitatsVersion.add({
	habitats : Habitats
});
module.exports = mongoose.model('HabitatsVersion', HabitatsVersion, 'HabitatsVersion');

