var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var FeedingAtomized = new Schema ({
	type : String,
	//thropic : [{ strategy : String }],
	thropic: { type: [{ strategy : String }], default: void 0 },
	ancillaryData: { type: [AncillaryData], default: void 0 }
},{ strict: false, versionKey: false });

var Feeding = new AbstractElementSchema();
Feeding.add({
	//feedingAtomized : [FeedingAtomized],
	feedingAtomized: { type: [FeedingAtomized], default: void 0 },
	feedingUnstructured : String
});


var FeedingVersion = new AbstractElementVersionSchema();
FeedingVersion.add({
	feeding : Feeding
});
module.exports = mongoose.model('FeedingVersion', FeedingVersion, 'FeedingVersion');

