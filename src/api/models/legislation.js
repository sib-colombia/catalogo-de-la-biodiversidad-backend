var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var LegislationAtomized = new AbstractElementSchema();
LegislationAtomized.add({
	legislationName : String,
	protectionLegalStatus : String,
	legislationRead : String,
	status : String,
	type : String,
	norm : String,
	appliesTo : {
		country : String,
		stateProvince : String,
		county : String,
		municipality : String,
		locality: String
	}
});

var Legislation = new AbstractElementSchema();
Legislation.add({
	legislationAtomized : [LegislationAtomized],
	legislationUnstructured : String
});

var LegislationVersion = new AbstractElementVersionSchema();
LegislationVersion.add({
	legislation : Legislation
});
module.exports = mongoose.model('LegislationVersion', LegislationVersion, 'LegislationVersion');

