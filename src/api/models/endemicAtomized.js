var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var EndemicAtomized = new AbstractElementSchema();
EndemicAtomized.add({
	endemicTo: [String],
	endemicIn : String,
});


var EndemicAtomizedVersion = new AbstractElementVersionSchema();
EndemicAtomizedVersion.add({
	endemicAtomized : [EndemicAtomized]
});
module.exports = mongoose.model('EndemicAtomizedVersion', EndemicAtomizedVersion, 'EndemicAtomizedVersion');

