var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var AncillaryData = require('mongoose').model('AncillaryData').schema;
var RecordVersion = require('mongoose').model('RecordVersion').schema;
//var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema


var MeasurementOrFact = new Schema({
	measurementID : String,
	measurementType : String,
	measurementValue : String,
	measurementAccuracy : String,
	measurementUnit : String,
	measurementDeterminedDate : String,
	measurementDeterminedBy: [String],
	measurementMethod : String,
	measurementRemarks : String,
	relatedTo : String
},{ collection : 'measurementOrFact', strict: false,  versionKey: false });

var Distance = new Schema({
	measurementOrFact : MeasurementOrFact,
	ancillaryData : AncillaryData
},{ versionKey: false });

var DispersalAtomized = new Schema ({
	purpose : String,
	type : { type: String },
	structureDispersed : String,
	distance : Distance,
	ancillaryData : AncillaryData
},{ versionKey: false });


var Dispersal = new AbstractElementSchema();
Dispersal.add({
	dispersalAtomized : DispersalAtomized,
	dispersalUnstructured : String
});


var DispersalVersion = new AbstractElementVersionSchema();
DispersalVersion.add({
	dispersal : Dispersal
});
module.exports = mongoose.model('DispersalVersion', DispersalVersion, 'DispersalVersion');


