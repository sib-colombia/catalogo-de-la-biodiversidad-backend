var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ad_objects = require('./additionalModels.js');
var Element = require('mongoose').model('Element').schema;
var ElementVersion = require('mongoose').model('ElementVersion').schema;
var MeasurementOrFact = require('mongoose').model('MeasurementOrFact').schema;
var AbstractElementVersionSchema = ad_objects.AbstractElementVersionSchema
var AbstractElementSchema = ad_objects.AbstractElementSchema

var UsesAtomized = new AbstractElementSchema();
UsesAtomized.add({
	sourceOfInformation : {
		references : [String],
		sourceOfInformationText : String
	},
	useValue : String,
	partUsed : String,
	users : String,
	organisms : String,
	vernacularNameUseAnnotations : String,
	productionDetails : String,
	meansOfApplicationAdministration : String,
	seasonOfAvailabilityUse : String,
	conservationExplotationData : String,
	useTypeAtomized : String,
	economics : String,
	ratingPopularity : String,
	properties : String,
	potential : String,
	useNotes : String
});

var Actions = new AbstractElementSchema();
Actions.add({
	measurementOrFact : [MeasurementOrFact]
});

var ManagementAndConservationAtomized = new AbstractElementSchema();
ManagementAndConservationAtomized.add({
	type : String,
	objetive : String,
	managementPlan : String,
	actions : [Actions],
	humanAndEnviromentalrelevanc : String
});

var ManagementAndConservation = new AbstractElementSchema();
ManagementAndConservation.add({
	managementAndConservationAtomized : [ManagementAndConservationAtomized],
	managementAndConservationUnstructured : String
});

var UsesManagementAndConservation = new AbstractElementSchema();
UsesManagementAndConservation.add({
	usesAtomized : [UsesAtomized],
	managementAndConservation : ManagementAndConservation
});


var UsesManagementAndConservationVersion = new AbstractElementVersionSchema();
UsesManagementAndConservationVersion.add({
	usesManagementAndConservation : UsesManagementAndConservation
});
module.exports = mongoose.model('UsesManagementAndConservationVersion', UsesManagementAndConservationVersion, 'UsesManagementAndConservationVersion');
/*
var UsesManagementAndConservationVersion = new Schema({
	name : String
},{ collection: 'UsesManagementAndConservationVersion', strict: false, versionKey: false });

module.exports = mongoose.model('UsesManagementAndConservationVersion', UsesManagementAndConservationVersion );
*/
