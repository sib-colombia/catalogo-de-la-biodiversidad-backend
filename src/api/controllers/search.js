import mongoose from 'mongoose';
import rp from 'request-promise';
import async from 'async';
import mongoosePaginate from 'mongoose-paginate';
import TaxonRecordNameVersion from '../models/taxonRecordName.js';
import add_objects from '../models/additionalModels.js';
import mapElements from '../models/elementNames.js';
import Property from '../models/property.js';
import { logger }  from '../../lib/logger';
import config from './../../config';

mongoose.Promise = require('bluebird');

var department_iso ={
  "CO-DC": "Bogotá Distrito Capital",
  "CO-AMA": "Amazonas",
  "CO-ANT": "Antioquia",
  "CO-ARA": "Arauca",
  "CO-ATL": "Atlántico",
  "CO-BOL": "Bolívar",
  "CO-BOY": "Boyacá",
  "CO-CAL": "Caldas",
  "CO-CAQ": "Caquetá",
  "CO-CAS": "Casanare",
  "CO-CAU": "Cauca",
  "CO-CES": "Cesar",
  "CO-COR": "Córdoba",
  "CO-CUN": "Cundinamarca",
  "CO-CHO": "Chocó",
  "CO-GUA": "Guainía",
  "CO-GUV": "Guaviare",
  "CO-HUI": "Huila",
  "CO-LAG": "La Guajira",
  "CO-MAG": "Magdalena",
  "CO-MET": "Meta",
  "CO-NAR": "Nariño",
  "CO-NSA": "Norte de Santander",
  "CO-PUT": "Putumayo",
  "CO-QUI": "Quindío",
  "CO-RIS": "Risaralda",
  "CO-SAP": "San Andrés, Providencia y Santa Catalina",
  "CO-SAN": "Santander",
  "CO-SUC": "Sucre",
  "CO-TOL": "Tolima",
  "CO-VAC": "Valle del Cauca",
  "CO-VAU": "Vaupés",
  "CO-VID": "Vichada"
}

function simpleSearchRecord(req, res) {
	if (req.swagger.params.q.value) {
		var qword = req.swagger.params.q.value;
    var numberRecords = req.swagger.params.size.value;
    var regexR1=/[^a-zA-Z0-9,:;óíáéúñ()\-\.\s]+/g;
    var alter_qword = qword.replace(regexR1,'').replace(/\s+/g," ");
		//var reg_ex = '.*'+qword+'.*';
    //var reg_ex = '^'+qword+'$';
    var reg_ex = '\\b'+qword+'\\b';
    var isCount = req.swagger.params.count.value;
    var query;
    if(isCount){
      query = add_objects.Record.find({$or:[ {'scientificNameSimple':{ '$regex': reg_ex, '$options' : 'i'}}, {'taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName': {'$regex': reg_ex, '$options' : 'i'}}, {'commonNames.name': {'$regex': reg_ex, '$options' : 'i'}}, {'abstractApprovedInUse.abstract': {'$regex': reg_ex, '$options' : 'i'}}, {'fullDescriptionApprovedInUse.fullDescription.fullDescriptionUnstructured': {'$regex': reg_ex, '$options' : 'i'}} ]}).select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1}).count();
    }else{
      query = add_objects.Record.find({$or:[ {'scientificNameSimple':{ '$regex': reg_ex, '$options' : 'i'}}, {'taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName': {'$regex': reg_ex, '$options' : 'i'}}, {'commonNames.name': {'$regex': reg_ex, '$options' : 'i'}}, {'abstractApprovedInUse.abstract': {'$regex': reg_ex, '$options' : 'i'}}, {'fullDescriptionApprovedInUse.fullDescription.fullDescriptionUnstructured': {'$regex': reg_ex, '$options' : 'i'}} ]}).select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1}).limit(numberRecords);
    }
  		query.exec(function (err, data) {
    		if(err){
      			logger.error('Error getting list of records', JSON.stringify({ message:err }) );
      			res.json(err);
    		}else if(data.length==0){
          /*
    			res.status(406);
      	  res.json({"message" : "Not found results for the simple search: "+req.swagger.params.q.value});
          */
          reg_ex = '\\b'+alter_qword+'\\b';


          if(isCount){
            query = add_objects.Record.find({$or:[ {'scientificNameSimple':{ '$regex': reg_ex, '$options' : 'i'}}, {'taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName': {'$regex': reg_ex, '$options' : 'i'}}, {'commonNames.name': {'$regex': reg_ex, '$options' : 'i'}}, {'abstractApprovedInUse.abstract': {'$regex': reg_ex, '$options' : 'i'}}, {'fullDescriptionApprovedInUse.fullDescription.fullDescriptionUnstructured': {'$regex': reg_ex, '$options' : 'i'}} ]}).select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1}).count();
          }else{
            query = add_objects.Record.find({$or:[ {'scientificNameSimple':{ '$regex': reg_ex, '$options' : 'i'}}, {'taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName': {'$regex': reg_ex, '$options' : 'i'}}, {'commonNames.name': {'$regex': reg_ex, '$options' : 'i'}}, {'abstractApprovedInUse.abstract': {'$regex': reg_ex, '$options' : 'i'}}, {'fullDescriptionApprovedInUse.fullDescription.fullDescriptionUnstructured': {'$regex': reg_ex, '$options' : 'i'}} ]}).select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1}).limit(numberRecords);
          }
          query.exec(function (err, data) {
            if(err){
              logger.error('Error getting list of records', JSON.stringify({ message:err }) );
              res.json(err);
            }else if(data.length==0){
              res.status(406);
              res.json({"message" : "Not found results for the simple search: "+req.swagger.params.q.value});
            }else{
            if(isCount){
                logger.info('Number or documents', JSON.stringify({ total: data }) );
                res.json({ total: data });
              }else{
                logger.info('Simple search', JSON.stringify({ total: data.length }) );
                res.json(data);
              }
              //logger.info('Simple search', JSON.stringify({ query: req.swagger.params.q.value }) );
              //res.json(data);
              }
            });
      		}else{
        			logger.info('Simple search', JSON.stringify({ query: req.swagger.params.q.value }) );
              if(isCount){
                res.json({ total: data });
              }else{
            		res.json(data);
              }

    		}
  		});
 	}else{
    res.json({message: "No query for search" });
 	}
}


async function advancedSearchRecord(req, res) {
  console.log("Advance Search----->",req.swagger.params)
  var query = {};
  var queryParam = {};
  var queryArray = [];
  var queryHierarchyArray = [];
  var num = 0;
  var num_hier = 0;
  var numberRecords = req.swagger.params.size.value;


  //console.log("Al inicio: ", req.swagger.params.threatCategory.value)




  if(req.swagger.params.scientificName.value){
    var scientificNameArray = req.swagger.params.scientificName.value;
    for(var i=0; i < req.swagger.params.scientificName.value.length; i++){
      scientificNameArray[i] = new RegExp('.*'+req.swagger.params.scientificName.value[i]+'.*', 'i');
    }
    queryArray[num]={'scientificNameSimple': { $in: scientificNameArray }};
    num++;
  }


  if(req.swagger.params.kingdom.value){
    var kingdomNameArray = req.swagger.params.kingdom.value;
    for(var i=0; i < req.swagger.params.kingdom.value.length; i++){
      kingdomNameArray[i] = new RegExp('.*'+req.swagger.params.kingdom.value[i]+'.*', 'i');
      //kingdomNameArray[i] = req.swagger.params.kingdom.value[i]
    }
    queryHierarchyArray[num_hier]={'hierarchy.kingdom': { $in: kingdomNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.phylum.value){
    var phylumNameArray = req.swagger.params.phylum.value;
    for(var i=0; i < req.swagger.params.phylum.value.length; i++){
      //phylumNameArray[i] = new RegExp('.*'+req.swagger.params.phylum.value[i]+'.*', 'i');
      phylumNameArray[i] = req.swagger.params.phylum.value[i]
    }
    //queryParam['hierarchy.kingdom']={ $in: kingdomNameArray };
    queryHierarchyArray[num_hier]={'hierarchy.phylum': { $in: phylumNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.class.value){
    var classNameArray = req.swagger.params.class.value;
    for(var i=0; i < req.swagger.params.class.value.length; i++){
      //classNameArray[i] = new RegExp('.*'+req.swagger.params.class.value[i]+'.*', 'i');
      classNameArray[i] = req.swagger.params.class.value[i]
    }
    //queryParam['hierarchy.kingdom']={ $in: kingdomNameArray };
    queryHierarchyArray[num_hier]={'hierarchy.classHierarchy': { $in: classNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.order.value){
    var orderNameArray = req.swagger.params.order.value;
    for(var i=0; i < req.swagger.params.order.value.length; i++){
      //orderNameArray[i] = new RegExp('.*'+req.swagger.params.order.value[i]+'.*', 'i');
      orderNameArray[i] = req.swagger.params.order.value[i]
    }
    //queryParam['hierarchy.kingdom']={ $in: kingdomNameArray };
    queryHierarchyArray[num_hier]={'hierarchy.order': { $in: orderNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.family.value){
    var familyNameArray = req.swagger.params.family.value;
    for(var i=0; i < req.swagger.params.family.value.length; i++){
      //familyNameArray[i] = new RegExp('.*'+req.swagger.params.family.value[i]+'.*', 'i');
      familyNameArray[i] = req.swagger.params.family.value[i]
    }
    //queryParam['hierarchy.kingdom']={ $in: kingdomNameArray };
    queryHierarchyArray[num_hier]={'hierarchy.family': { $in: familyNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.genus.value){
    var genusNameArray = req.swagger.params.genus.value;
    for(var i=0; i < req.swagger.params.genus.value.length; i++){
      //genusNameArray[i] = new RegExp('.*'+req.swagger.params.genus.value[i]+'.*', 'i');
      genusNameArray[i] = req.swagger.params.genus.value[i]
    }
    queryHierarchyArray[num_hier]={'hierarchy.genus': { $in: genusNameArray  }};
    num_hier++;
  }

  if(req.swagger.params.subGenus.value){
    var subGenusNameArray = req.swagger.params.subGenus.value;
    for(var i=0; i < req.swagger.params.subGenus.value.length; i++){
      //subGenusNameArray[i] = new RegExp('.*'+req.swagger.params.subGenus.value[i]+'.*', 'i');
      subGenusNameArray[i] = req.swagger.params.subGenus.value[i]
    }
    queryHierarchyArray[num_hier]={'hierarchy.subgenus': { $in: subGenusNameArray  }};
    num_hier++;
  }


  if(queryHierarchyArray.length !=0){
    queryArray[num]= {$or:queryHierarchyArray};
    num++;
  }



  if(req.swagger.params.department.value){
    var departmentNameArray = req.swagger.params.department.value;
    for(var i=0; i < req.swagger.params.department.value.length; i++){
      //departmentNameArray[i] = new RegExp('.*'+department_iso[req.swagger.params.department.value[i]]+'.*', 'i');
      departmentNameArray[i] = department_iso[req.swagger.params.department.value[i]]
    }
    queryArray[num]={'distributionApprovedInUse.distribution.distributionAtomized.stateProvince': { $in: departmentNameArray }};
    num++;
  }





  /*
  if(req.swagger.params.department.value){
    var departmentNameArray = req.swagger.params.department.value;
    console.log(req.swagger.params.department.value.length);
    for(var i=0; i < req.swagger.params.department.value.length; i++){
      departmentNameArray[i] = new RegExp('.*'+req.swagger.params.department.value[i]+'.*', 'i');
    }
    queryArray[num]={'distributionApprovedInUse.distribution.distributionAtomized.stateProvince': { $in: departmentNameArray }};
    num++;
  }*/



  /*
  if(req.swagger.params.threatCategory.value){
    var threatCategoryArray2 = [] //= req.swagger.params.threatCategory.value;
    console.log(req.swagger.params.threatCategory.value.length);
    for(var i=0; i < req.swagger.params.threatCategory.value.length; i++){
      threatCategoryArray2[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
    }
    queryArray[num]={'threatStatusApprovedInUse.threatStatus.threatStatusAtomized.threatCategory.measurementValue': { $in: threatCategoryArray2 }};
    num++;
  }

  console.log("Estado de amenaza - threatCategory ", req.swagger.params.threatCategory)
  if(req.swagger.params.threatCategory.value){
    var queryThreatCategoryArray = [];
    var threatCategoryArray = req.swagger.params.threatCategory.value;
    for(var i=0; i < req.swagger.params.threatCategory.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      threatCategoryArray[i] = req.swagger.params.threatCategory.value[i];
    }
    queryThreatCategoryArray[0]={'threatStatusApprovedInUse.threatStatus.threatStatusAtomized.threatCategory.measurementValue': { $in: threatCategoryArray  }};
    queryArray[num]= {$or:queryThreatCategoryArray};

    num++;
  }


  console.log("Estado de amenaza - ApendiceCITES ", req.swagger.params.apendiceCITES)
  if(req.swagger.params.apendiceCITES.value){
    var queryApendiceCITESArray = [];
    var apendiceCITESArray = req.swagger.params.apendiceCITES.value;
    for(var i=0; i < req.swagger.params.apendiceCITES.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      apendiceCITESArray[i] = req.swagger.params.apendiceCITES.value[i];
    }
    console.log("apendiceCITESArray")
    console.log(apendiceCITESArray)

    queryApendiceCITESArray[0]={'threatStatusApprovedInUse.threatStatus.threatStatusAtomized.apendiceCITES': { $in: apendiceCITESArray  }};
    queryArray[num]= {$or:queryApendiceCITESArray};

    console.log("queryApendiceCITESArray")
    console.log(queryApendiceCITESArray)


    num++;
  }
  */


  //threatUICN
  //console.log("Estado de amenaza - threatCategory threatUICN", req.swagger.params.threatUICN)

  //console.log("ASSESMENT-------------->",req.swagger.params.assessment.value)

  if(req.swagger.params.threatUICN.value){
    var queryThreatCategoryArray = [];
    var threatCategoryArray = req.swagger.params.threatUICN.value;
    for(var i=0; i < req.swagger.params.threatUICN.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      threatCategoryArray[i] = req.swagger.params.threatUICN.value[i];
    }

    console.log("threatCategoryArray",threatCategoryArray)
    queryThreatCategoryArray[0]={
      'threatStatusApprovedInUse.threatStatus':
        {
          $elemMatch:
            {
            'threatStatusAtomized.threatCategory.measurementValue': { $in: threatCategoryArray  },
            'threatStatusAtomized.threatCategory.measurementType': 'UICN'
            }
        }
      };
    queryArray[num]= {$or:queryThreatCategoryArray};

    num++;
  }


  if(req.swagger.params.usos.value){
    var queryUsosArray = [];
    var usosArray = req.swagger.params.usos.value;
    for(var i=0; i < req.swagger.params.usos.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      if (req.swagger.params.usos.value[i]==='PFNM'){
        queryUsosArray.push({
            "source.PFNM": "true"
          }
        )
      }else if (req.swagger.params.usos.value[i]==='Otros'){
        queryUsosArray.push({
          "usesManagementAndConservationApprovedInUse.usesManagementAndConservation.usesAtomized": {$exists: true, $not: {$size: 0}},
          "source.PFNM": "false"
          }
        )
      }
    }
    queryArray[num]= {$or:queryUsosArray};
    num++;
  }


  if(req.swagger.params.assessment.value){
    var filesIds = []
    await rp({uri: config['risk-extintion-api']+'/iucn/cbc',    json: true})
      .then((files) => {
        files.forEach(element =>{
          if (element.ident.CBC.length===24){
            //console.log(element.ident.CBC)
            filesIds.push(element.ident.CBC);
          }
        });
      });
      
    //console.log(filesIds)

    var queryRiskAndExtinctArray = [];
    queryRiskAndExtinctArray[0]={'_id': { $in:  filesIds }};
    queryArray[num]= {$or:queryRiskAndExtinctArray};
    num++;
  }


  //threatMADS
  //console.log("Estado de amenaza - threatCategory threatMADS", req.swagger.params.threatMADS)
  if(req.swagger.params.threatMADS.value){
    var queryThreatCategoryArray = [];
    var threatCategoryArray = req.swagger.params.threatMADS.value;
    for(var i=0; i < req.swagger.params.threatMADS.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      threatCategoryArray[i] = req.swagger.params.threatMADS.value[i];
    }
    queryThreatCategoryArray[0]={
      'threatStatusApprovedInUse.threatStatus':
        {
          $elemMatch:
            {
            'threatStatusAtomized.threatCategory.measurementValue': { $in: threatCategoryArray  },
            'threatStatusAtomized.threatCategory.measurementType': 'MADS'
            }
        }
      };
    queryArray[num]= {$or:queryThreatCategoryArray};

    num++;
  }
  //threatCITES
  //console.log("Estado de amenaza - threatCITES ", req.swagger.params.threatCITES)
  if(req.swagger.params.threatCITES.value){
    var queryApendiceCITESArray = [];
    var apendiceCITESArray = req.swagger.params.threatCITES.value;
    for(var i=0; i < req.swagger.params.threatCITES.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      apendiceCITESArray[i] = req.swagger.params.threatCITES.value[i];
    }
    console.log("apendiceCITESArray")
    console.log(apendiceCITESArray)

    queryApendiceCITESArray[0]={'threatStatusApprovedInUse.threatStatus.threatStatusAtomized.apendiceCITES': { $in: apendiceCITESArray  }};
    queryArray[num]= {$or:queryApendiceCITESArray};

    num++;
  }


  //habitat
  //console.log("Estado de amenaza - habitat ", req.swagger.params.habitat)
  if(req.swagger.params.habitat.value){
    console.log("habitat------->",req.swagger.params.habitat.value)
    var queryHabitatArray = [];
    var habitatArray = [];
    for(var i=0; i < req.swagger.params.habitat.value.length; i++){
      //threatCategoryArray[i] = new RegExp('.*'+req.swagger.params.threatCategory.value[i]+'.*', 'i');
      habitatArray[i] = req.swagger.params.habitat.value[i];
    }
    console.log("queryHabitatArray")
    console.log(queryHabitatArray)

    queryHabitatArray[0]={'habitats_version.habitats.habitatAtomized.measurementOrFact.measurementValue': { $in: habitatArray  }};
    queryArray[num]= {$or:queryHabitatArray};

    num++;
  }







  /*
  if(req.swagger.params.multimedia.value){
    var multimediaArray = req.swagger.params.multimedia.value;
    console.log(req.swagger.params.multimedia.value.length);
    for(var i=0; i < req.swagger.params.multimedia.value.length; i++){
      multimediaArray[i] = req.swagger.params.multimedia.value[i];
    }
    queryArray[num]={'ancillaryDataApprovedInUse.ancillaryData.dataType': { $in: multimediaArray }};
    num++;
  }
  */

  if(req.swagger.params.multimedia.value){
    var queryMultimediaArray = [];
    var multimediaArray = req.swagger.params.multimedia.value;
    for(var i=0; i < req.swagger.params.multimedia.value.length; i++){
      multimediaArray[i] = req.swagger.params.multimedia.value[i];
    }
    queryMultimediaArray[0]={'ancillaryDataApprovedInUse.ancillaryData.dataType': { $in: multimediaArray }};
    if(multimediaArray.includes('image')){
      queryMultimediaArray[1]={'imageInfo': {$exists: true}};
    }
    queryArray[num]= {$or:queryMultimediaArray};
    num++;
  }


  //console.log("El valor de los parámetros es ", req.swagger.params)
  if(req.swagger.params.invasiveness.value){
    queryArray[num]={'invasivenessApprovedInUse.invasiveness.invasivenessUnstructured': {$exists: true, $not: {$size: 0}, $ne: '' }};
    num++;
  }

  if(req.swagger.params.exotic.value){
    console.log("exooooticas")
    queryArray[num]={'originAtomized.exotic': 'true' };
    num++;
  }

  if(req.swagger.params.invasive.value){
    queryArray[num]={'originAtomized.invasive': 'true' };
    num++;
  }


  console.log("queryArray: ", JSON.stringify(queryArray))


  var isCount = req.swagger.params.count.value;
  mongoose.set('debug', true);

  if(queryArray.length > 0){
    if(isCount){
      query = add_objects.Record.find({$and:queryArray})//{$and:queryArray}
        .select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date threatStatusApprovedInUse fullDescriptionApprovedInUse originAtomized habitats_version taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1})
        .count();
    }else{
      query = add_objects.Record.find({$and:queryArray})
        .select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date threatStatusApprovedInUse fullDescriptionApprovedInUse originAtomized habitats_version taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1})
        .sort({update_date: -1})
        .limit(numberRecords);
    }
  }else{
    query = add_objects.Record.find({}).select('_id scientificNameSimple imageInfo threatStatusValue commonNames creation_date update_date threatStatusApprovedInUse fullDescriptionApprovedInUse originAtomized habitats_version taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalName.simple taxonRecordNameApprovedInUse.taxonRecordName.scientificName.canonicalAuthorship.simple').sort({update_date: -1}).count();
    isCount = true;
  }

  console.log("Consulta: ")
  console.log(query)

  query.exec(function (err, data) {
    if(err){
      logger.error('Error getting list of records', JSON.stringify({ message:err }) );
      res.json(err);
    }else if(data.length==0){
      res.status(406);
      res.json({"message" : "Not found results for the advancedSearchRecordced search"});
    }else{
      if(isCount){
        logger.info('Number or documents Count', JSON.stringify({ total: data }) );
        logger.info(JSON.stringify({ total: data }) );
        res.json({ total: data });
      }else{
        logger.info('Number or documents Find', JSON.stringify({ total: data.length }) );
        logger.info(JSON.stringify({ total: data.length }) );
        res.json(data);
      }
    }
  });


}


module.exports = {
  simpleSearchRecord,
  advancedSearchRecord
};
