# Backend SIM

API REST Portal de información de Catálogo de la Biodiversidad del SiB Colombia


### Tecnologías

Implementado sobre API https://github.com/kevoj/nodetomic-api-swagger/

> RESTful API Nodejs designed for horizontal scalability with support for
cluster, based on Swagger, Redis, JWT, Passport, Socket.io, Express, MongoDB.

### Preview

**url:** <http://localhost:8000/socket>

[![](https://j.gifs.com/0gw5gX.gif)](https://www.youtube.com/watch?v=drhEQl-4x0w)

### Swagger Api

**url:** <http://localhost:8000/docs> 

**OpenAPI Spec compatibility:** 2.0, 3.0

<img src="https://i.imgur.com/lnOKBF4.png">

### Escalabilidad horizontal

View horizontal scaling representation image with nodetomic-api-swagger <a href="https://github.com/kevoj/nodetomic-api-swagger/wiki/1.-Horizontal-scalability">HERE!</a>

### Structure

<pre> 
/src/
|-- api
|   |-- swagger
|   |-- controllers
|   |-- models
|   `-- sockets
|-- assets
|-- auth
|   |-- swagger
|   |-- controllers
|   |-- passports
|   `-- services
|-- config
|-- lib
|   |-- express
|   |-- mongoose
|   |-- redis-jwt
|   |-- socket.io
|   `-- swagger
|-- views
`-- app.js
</pre> 

## Requirimientos

- [Nodejs](https://nodejs.org) >= **12.x.x**
- [MongoDB](https://www.mongodb.com)  >= **3.x.x**
- [Redis](https://redis.io)  >= **3.x.x** (Recommended last version)
- [pm2](https://pm2.keymetrics.io/)  >= **5.x.x** 
  - Install as a global npm dependency `npm install -g pm2`

## Instalación

**npm**

```bash
git clone https://gitlab.com/sib-colombia/catalogo-de-la-biodiversidad-backend.git
cd catalogo-de-la-biodiversidad-backend
npm install
```

**Yarn (out of date)**

```bash
git clone https://gitlab.com/sib-colombia/catalogo-de-la-biodiversidad-backend.git
cd catalogo-de-la-biodiversidad-backend
yarn install sim-backend
```

## Development

### Start

**Configurar ip:** Ir al archivo `src/config/index.js` y asegurarse que la ip donde será deplegado el servidor sea `127.0.0.1` o `localhost`

**Comando:** `npm start` 

**Descripción:** Inicia el proyecto en modo desarrollo:

![Imgur](https://i.imgur.com/qY1mzDZ.png)

**Note:** Si quiere trabajar con **nodemon** se puede ejecutar el comando `npm run modemon`

### Build

**Comando:** `npm run build`

**Descripción:** Compilar el proyecto y generando la carpeta dist

![Imgur](http://i.imgur.com/yTI3otr.png)

**Note:** Genera la carpeta **`dist`**. También "dist/client" que es opcional. Este componente puede ser sim-frontend.

![Imgur](https://i.imgur.com/bVFqr1f.png)

### Test

**Comando:** `npm test`

**Descripción:** Ejecuta Lint y ejecuta el Build en modo producción y ejecuta los comandos de test.

![Imgur](http://i.imgur.com/ouKpQg1.png)

### Lint

**Comando:** `npm run lint`

**Descripción:** Ejecuta ESLint para verificar el proyecto completo.

<br>

## Pm2 [Desarrollo]

### Dev-Simple

**Comando:** `npm run dev-simple`

**Descripción:** Ejecuta Pm2 y compila el proyecto en modo desarrollo en una instancia simple.

![Imgur](http://i.imgur.com/cNuBVzK.png)

### Dev-Cluster

**Comando:** `npm run dev-cluster`

**Descripción:** Ejecuta Pm2 y compila el proyecto en modo desarrollo en multiples instancias.

![Imgur](http://i.imgur.com/wEU2Uz5.png)

## Pm2 [Producción]

### Simple

**Comando:** `npm run simple`

**Descripción:** Ejecuta Pm2 y compila el proyecto en modo producción en una instancia simple.

![Imgur](http://i.imgur.com/tLA2hu7.png)

### Cluster

**Comando:** `npm run cluster`

**Descripción:** Ejecuta Pm2 y compila el proyecto en modo producción en multiples instancias.

![Imgur](http://i.imgur.com/HTWJcUk.png)

## Stop

### Pm2

**Comando:** `npm stop`

**Descripción:** Detiene todos los procesos asociados con el proyecto en pm2.

### Node

**Comando:** `killall node`

**Descripción:** Detiene todos los procesos de node.

## API Docs desarrollo

Documentación del proyecto base: <a href="https://github.com/kevoj/nodetomic-api-swagger/wiki" >Acá!</a>

## License

MIT © [Leonardo Rico](https://github.com/kevoj/nodetomic-api-swagger/blob/master/LICENSE)
