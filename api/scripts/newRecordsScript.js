var mongoose = require('mongoose');
var async = require('async');
var fs = require('fs');
var _ = require('lodash')
var TaxonRecordNameVersion = require('../../src/api/models/taxonRecordName.js');
var SynonymsAtomizedVersion = require('../../src/api/models/synonymsAtomized.js');
var CommonNamesAtomizedVersion = require('../../src/api/models/commonNamesAtomized.js');
var AncillaryDataVersion = require ('../../src/api/models/ancillaryData.js');
var HierarchyVersion = require('../../src/api/models/hierarchy.js');
var ThreatStatusVersion = require('../../src/api/models/threatStatus.js');
var HabitatsV =  require('../../src/api/models/habitats.js')
var add_objects = require('../../src/api/models/additionalModels.js');
var parse = require('csv-parse');
var rest = require('restler');
var request = require('request')
var Schema = mongoose.Schema;

require('dotenv').config()


//var CatalogoDb = mongoose.createConnection('mongodb://localhost:27017/chigui', function(err) {
var CatalogoDb = mongoose.createConnection(process.argv[2], { useNewUrlParser: true }, function(err) {
//var CatalogoDb = mongoose.createConnection('mongodb://192.168.11.92:27017/chigui', function(err) {
	var newRecordSchema = add_objects.Record.schema;
	var Record = CatalogoDb.model('Record', newRecordSchema );
	if(err){
		console.log('connection error', err);
	}else{
    console.log('****Initial waterfal async****');
		async.waterfall([

		  //Borrar esta función en producción, es para desarrollo e ir probando de a un registro
		  // function(callback){
		  //   console.log("\n\n\n==== Limpieza inciial de la colección, temporal en desarrollo OJO quitar en producción\n\n\n\n")
		  //   Record.remove({}, function(){
		  //     callback(null)
		  //   })
		  // },


			function(callback){
    			console.log("Step 1:Read the csv file ");
        		//RecordModel.find({}).exec(callback);
        		//Leer el archivo, Read the file
        		var data = [];
        		var input = fs.createReadStream(process.argv[3]);
        		var parser = parse({delimiter: ','});
        		parser.on('readable', function(){
  					 while(record = parser.read()){
    					data.push(record);
  					 }
				    });
				    parser.on('finish', function(){
					   callback(null, data);
				    });
				    input.pipe(parser);
        		//var stream = fs.createReadStream(inputFile).pipe(parser);

        	},
        	function(data,callback){
            console.log('Step 2: Asyn series for each line in the document ');
        		console.log('Number of scientific namesUpdate Record images\n to insert: '+data.length);

            var recordVersionSchema = add_objects.RecordVersion.schema;
            var RecordVersion = CatalogoDb.model('RecordVersion', recordVersionSchema, "RecordVersion");

            var taxonSchema = TaxonRecordNameVersion.schema;
            TaxonRecordNameVersion = CatalogoDb.model('TaxonRecordNameVersion', taxonSchema, "TaxonRecordNameVersion");

            var hierarchySchema = HierarchyVersion.schema;
            HierarchyVersion = CatalogoDb.model('HierarchyVersion', hierarchySchema, "HierarchyVersion");

            var ancillarySchema = AncillaryDataVersion.schema;
            AncillaryDataVersion = CatalogoDb.model('AncillaryDataVersion', ancillarySchema, "AncillaryDataVersion");

            var threatStatusSchema = ThreatStatusVersion.schema;
            ThreatStatusVersion = CatalogoDb.model('ThreatStatusVersion', threatStatusSchema, "ThreatStatusVersion");


            HabitatsVersion =CatalogoDb.model('HabitatsVersion', HabitatsV.schema, 'HabitatsVersion');

            var number_new_records=0;
            var number_images_updated=0;

          	data=data.slice(1, data.length);

              var canName	= ''
              var scientificNameAuthorship	= ''
              var sciName	= ''
              var kingdom	= ''
              var phylum	= ''
              var class_es	= ''
              var order	= ''
              var family	= ''
              var genus	= ''
              var specificEpithet	= ''
              var infraspecificEpithet	= ''
              var taxonRank	= ''
              var ident_UICN	= ''
              var authority_global	= ''
              var appliesTo_global	= ''
              var threatCategory_global	= ''
              var authority_nacional	= ''
              var appliesTo_nacional	= ''
              var threatCategory_nacional	= ''
              var appendixCITESI	= ''
              var appendixCITESII	= ''
              var appendixCITESIII	= ''
              var habitatparamo	= ''
              var habitathumedal	= ''
              var endemic	= ''
              var imagenDestacada	= ''
              var imagenMiniatura	= ''
              var license	= ''
              var source	= ''
              var agent	= ''
              var rightsHolder	= ''
              var autor	= ''
              var originAtomizedexotic	= ''
              var originAtomizedinvasive	= ''
              var registros_GBIF	= ''
              var lista_ref_tax	= ''
              var lista_MADS	= ''
              var lista_GRIIS	= ''
              var lista_roja_UICN	= ''
              var PIFS	= ''
              var PIFS_grupoBio	= ''
              var PFNM	= ''

              var apiUICN=undefined


          		async.eachSeries(data, function(line, callback) {
                console.log("Values to save");
          			console.log(line);
                //var serialArr = JSON.parse(line);
                //console.log("value 0: "+serialArr[0]);
          			//line = line+"";
          			//specie =line.split(",");

          	    //speciesKey,
          	    //canonicalName,

                let extra = -1


                canName = line[0]
                scientificNameAuthorship = line[1]
                sciName = line[2]
                kingdom = line[3]
                phylum = line[4]
                class_es = line[5]
                order = line[6]
                family = line[7]
                genus = line[8]
                specificEpithet = line[9]
                infraspecificEpithet = line[10]
                taxonRank = line[11+extra]
                ident_UICN = line[12+extra]
                authority_global = line[13+extra]
                appliesTo_global = line[14+extra]
                threatCategory_global = line[15+extra]
                authority_nacional = line[16+extra]
                appliesTo_nacional = line[17+extra]
                threatCategory_nacional = line[18+extra]
                appendixCITESI = line[19+extra]
                appendixCITESII = line[20+extra]
                appendixCITESIII = line[21+extra]
                habitatparamo = line[22+extra]
                habitathumedal = line[23+extra]
                endemic = line[24+extra]
                imagenDestacada = line[25+extra]
                imagenMiniatura = line[26+extra]
                license = line[27+extra]
                source = line[28+extra]
                agent = line[29+extra]
                rightsHolder = line[30+extra]
                autor = line[31+extra]
                originAtomizedexotic = line[32+extra]
                originAtomizedinvasive = line[33+extra]
                registros_GBIF = line[34+extra]
                lista_ref_tax = line[35+extra]
                lista_MADS = line[36+extra]
                lista_GRIIS = line[37+extra]
                lista_roja_UICN = line[38+extra]
                PIFS = line[39+extra]
                PIFS_grupoBio = line[40+extra]
                PFNM = line[41+extra]
                
                console.log("El archivo es: ", imagenMiniatura)

                var Record_source = {
                  registros_GBIF: registros_GBIF,
                  lista_ref_tax: lista_ref_tax,
                  lista_MADS: lista_MADS,
                  lista_GRIIS: lista_GRIIS,
                  lista_roja_UICN: lista_roja_UICN,
                  PFNM: PFNM
                }
                var Record_originAtomized= {
                  exotic: originAtomizedexotic,
                  invasive: originAtomizedinvasive
                }

                console.log("\n\n\n\n\nIntentando procesar ", Record_source, "\n\n\n\n\n")



                var hierarchyArray = [];

          			console.log("canonical name to search: "+canName);
          			/*
          			var name='zantedeschia aethiopicax';
          			var image = 'test.jpg';
          			*/
          			//console.log(sciName);
                //canName ='zantedeschia aethiopicaxztqwbhlp';
          			var reg_ex = '^'+canName;
                console.log("waterfall for each line");
          			async.waterfall([


          			  //Consulta al api IUCN si la variable tiene algún valor
          			  // function(callback){
          			  //   console.log("\n\n\n===== Consulta del API UICN ", ident_UICN, process.env.TOKEN_UICN)
          			  //   if (ident_UICN  !==""){
                  //     console.log("-----------  https://apiv3.iucnredlist.org/api/v3/species/id/"+ident_UICN+"?token="+process.env.TOKEN_UICN)
                  //     request({
                  //             url: "https://apiv3.iucnredlist.org/api/v3/species/id/"+ident_UICN+"?token="+process.env.TOKEN_UICN,
                  //             method: "GET"
                  //         }, (error, res, body) => {
                  //       apiUICN = JSON.parse(body)
                  //       callback(null)
            			//     })
          			  //   }else{
            			//     callback(null)
          			  //   }
                  //
          			  // },


          				function(callback){
          					console.log("Step 2.1: Search by canonicalName: "+canName);
          					Record.findOne({'scientificNameSimple': {'$regex' : reg_ex, '$options' : 'i'} }, 'scientificNameSimple', function(err, record){
            					console.log("############ el registro anterior es ", record)
          						if(err){
          							console.log("Error finding scientificName in the database!: " + canName);
									       callback(new Error("Error to get EcologicalSignificance element for the record with id: "+id+" : " + err.message));
          						}else{
          							if(record){
                          console.log("!!!Exist a record for the canonicalName: "+canName+" id: "+record._id);
          								callback(null, record._id);
          							}else{
          								console.log("No exist record for the canonicalName: "+canName);
          								callback(null, '');
          							}
          						}
          					});
          				},
          				function(id,callback){
                    console.log("Step 2.2: create new elements and record if not exist Record, in other case search by id: "+id);
          					var taxonRecordName = {};
                    var scientificName = {};
                    var canonicalName = {};
                    var create_new_record = false;
          					if(id === ''){
                      scientificName.simple = sciName;
                      scientificName.rank = "SPECIES";
                      canonicalName.simple = canName;
                      scientificName.canonicalName = canonicalName;
                      taxonRecordName.scientificName = scientificName;
								      create_new_record=true;
          					}
                    callback(null,taxonRecordName,id, create_new_record);
          				},
          				function(taxonRecordName, id, create_new_record, callback){
                    console.log("Step 2.2: create a new Record?:"+create_new_record +" id: "+id);
          					if(create_new_record){
                      console.log(JSON.stringify(taxonRecordName));
          						var id_rc = mongoose.Types.ObjectId();
								      var id_v = mongoose.Types.ObjectId();
                      var taxon_record_name_version = {};
								      taxon_record_name_version._id = id_v;
								      taxon_record_name_version.id_record=id_rc;
								      taxon_record_name_version.created=Date();
								      taxon_record_name_version.state="approved_in_use";
								      taxon_record_name_version.element="taxonRecordName";
								      taxon_record_name_version.id_user="sib+ac@humboldt.org.co";
                      var taxonRecordNameElement = taxon_record_name_version;
                      taxon_record_name_version.taxonRecordName = taxonRecordName;
								      taxon_record_name_version = new TaxonRecordNameVersion(taxon_record_name_version);
								      var ver = 1;
                      var ob_ids= new Array();
                      ob_ids.push(id_v);
								      RecordVersion.create({ _id:id_rc, taxonRecordNameVersion: ob_ids},function(err, doc){
									     if(err){
										    console.log("Error creating a new RecordVersion for the name: " + canName);
										    callback(new Error("Error creating a new RecordVersion for the name: " + canName +" : " + err.message));
									     }else{
                        console.log("Create a new TaxonRecordNameVersion");
                        taxon_record_name_version.version=1;
                        taxon_record_name_version.save(function(err){
                          if(err){
                            console.log("Error creating a new taxonRecordNameVersion for the name: " + canName);
                            callback(new Error("Error creating a new taxonRecordNameVersion for the name: " + canName +" : " + err.message));
                          }else{
                            var update_date = Date();
                            var scientificNameSimple = taxonRecordName.scientificName.simple;
                            
                            
                  					console.log("############ Creando un nuevo registro  _id:", id_rc)
                  					console.log("------------- ", {source: Record_source})

                            Record.create({ _id:id_rc, taxonRecordNameApprovedInUse: taxon_record_name_version, scientificNameSimple: scientificNameSimple, source: Record_source, update_date: update_date, creation_date: update_date},function(err, doc){
                              if(err){
                                console.log("Error creating a new Record for the name: " + canName);
                                callback(new Error("Error creating a new Record for the name: " + canName +" : " + err.message));
                              }else{
                                number_new_records++;
                                console.log("Created a new Record id: " + id_rc + "; Created a new taxonRecordNameVersion, id: "+id_v);
                                callback(null,id_rc, create_new_record);
                              }
                            });
                          }
                        });
									     }
								      });
          					}else{
                      console.log("Not created a new Record");
                      callback(null,id, create_new_record);
          					}
          				},
                  function(id, create_new_record, callback){
                    console.log("Step 2.3: Create hierarchy object, create a new HierarchyVersion?:"+create_new_record +" id: "+id);
                    hierarchy = [];
                    var hierarchyVal = {};
                    if(create_new_record){
                      console.log(create_new_record);
                      console.log(id);
                      hierarchyVal.kingdom = kingdom;
                      hierarchyVal.phylum = phylum;
                      hierarchyVal.classHierarchy = class_es;
                      hierarchyVal.order = order;
                      hierarchyVal.family = family;
                      hierarchyVal.genus = genus;
                      hierarchyVal.specificEpithet = specificEpithet;
                      hierarchy.push(hierarchyVal);
                    }
                    callback(null,id, create_new_record, hierarchy);
                  },
                  function(id, create_new_record, hierarchy, callback){
                    console.log("Step 2.4: create a new HierarchyVersion?:"+create_new_record +" id: "+id);
                    //console.log(JSON.stringify(hierarchy));
                    if(create_new_record){
                      var hierarchy_version = {};
                      var id_v = mongoose.Types.ObjectId();
                      hierarchy_version._id = id_v;
                      hierarchy_version.created=Date();
                      hierarchy_version.state="approved_in_use";
                      hierarchy_version.id_user = 'sib+ac@humboldt.org.co';
                      hierarchy_version.element="hierarchy";
                      hierarchyArray = hierarchy;
                      hierarchy_version.hierarchy = hierarchy;
                      //var elementValue = hierarchy_version.hierarchy;
                      hierarchy_version = new HierarchyVersion(hierarchy_version);
                      var id_v = hierarchy_version._id;
                      async.waterfall([
                        function(callback){
                          hierarchy_version.id_record=id;
                          hierarchy_version.version=1;
                          hierarchy_version.save(function(err){
                            if(err){
                              callback(new Error("failed saving the element version:" + err.message));
                            }else{
                              callback(null, hierarchy_version);
                            }
                          });
                      },
                      function(hierarchy_version, callback){
                        console.log("id to update:"+id);
                        RecordVersion.findByIdAndUpdate( id, { $push: { "hierarchyVersion": id_v } },{ safe: true, upsert: true }).exec(function (err, record) {
                          if(err){
                            console.log(err.message);
                            callback(new Error("failed added id to RecordVersion:" + err.message));
                          }else{
                            console.log('!!');
                            callback();
                          }
                        });
                      },
                      function(callback){
                        var update_date = Date();
                        console.log("######## UPDATE 392 ############## ", Record_source)
                        Record.update({_id:id},{ hierarchyApprovedInUse: hierarchy_version, update_date: update_date, source: Record_source, originAtomized: Record_originAtomized }, function(err, result){
                          console.log("Registro almacenado: ", result)
                          if(err){
                            callback(new Error(err.message));
                          }else{
                            callback();
                          }
                        });
                      }
                      ],
                      function(err, result) {
                        if (err) {
                          callback(new Error("failed saving HierarchyVersion for the new record:" + err.message));
                        }else{
                          console.log("Created a new HierarchyVersion id: " +id_v);
                          callback(null, id, create_new_record);
                        }
                      });
                    }else{
                      callback(null, id, create_new_record);
                    }
                  },
                  function(id, create_new_record, callback){
                    console.log("Step 2.5: Update image for a new Record?:"+create_new_record +" id: "+id);
                    console.log("id to update image: "+id);
                    if((imagenDestacada == '')&&(imagenMiniatura == '')){
                      console.log('Not exists images to update or save');
                      callback();
                    }else{
                      var ancillaryData = [];
                      var mediaURL = [];
                      var ancillaryDataValue = {};


                      ancillaryDataValue.source = source;
                      ancillaryDataValue.agent = {lastName: agent};
                      ancillaryDataValue.thumbnailURL = imagenMiniatura;
                      mediaURL.push(imagenDestacada);
                      ancillaryDataValue.mediaURL = mediaURL;
                      ancillaryDataValue.rightsHolder = rightsHolder;
                      ancillaryDataValue.rights = autor;
                      ancillaryDataValue.license = license;
                      ancillaryData.push(ancillaryDataValue);
                      var imageInfo = {};
                      imageInfo.mainImage = imagenDestacada;
                      imageInfo.thumbnailImage = imagenMiniatura;
                      imageInfo.source = source;
                      imageInfo.rightsHolder = rightsHolder;
                      imageInfo.rights = autor;
                      imageInfo.license = license;
                      console.log(JSON.stringify(ancillaryData));
                      var ancillary_data_version = {}
                      var threat_status_version = {}
                      var id_v = mongoose.Types.ObjectId();
                      ancillary_data_version._id = id_v;
                      ancillary_data_version.created=Date();
                      ancillary_data_version.state="approved_in_use";
                      ancillary_data_version.element="ancillaryData";
                      ancillary_data_version.id_user = 'sib+ac@humboldt.org.co';
                      ancillary_data_version.ancillaryData = ancillaryData;
                      ancillary_data_version = new AncillaryDataVersion(ancillary_data_version);


          //Este objeto toca construirlo si no existe, y si existe actualizarlo
                      threat_status_version._id = id_v;
                      threat_status_version.created=Date();
                      threat_status_version.state="approved_in_use";
                      threat_status_version.element="threatStatus";
                      threat_status_version.id_user = 'sib+ac@humboldt.org.co';
                      threat_status_version.threatStatus = []


                      if (appendixCITESI!=="" || appendixCITESII!=="" || appendixCITESIII!==""){
                          threat_status_version.threatStatus.push({
                            "threatStatusAtomized": {
                              "apendiceCITES": _.compact([appendixCITESI, appendixCITESII, appendixCITESIII])
                            }
                          })
                      }

                      if (ident_UICN!==""){
                        console.log("-----Se debe actualizar un threat_status_version: ")
                        /*
                        SI: (existe "ident_UICN" en CSV)
                        - Buscar en el API de IUCN el campo "CATEGORY: API IUCN".
                          - Llenar los campos del Plinian-Core.
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.threatCategory.measurementValue = "(Campo CATEGORY: API IUCN)"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.appliesTo.country = "GLOBAL"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.authority = "IUCN"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.threatCategory.measurementType = "IUCN"
                        */

                        console.log(typeof apiUICN)
                        if (threatCategory_global){
                          threat_status_version.threatStatus.push({
                            "threatStatusAtomized": {
                              "threatCategory": {
                                "measurementValue": threatCategory_global,
                                "measurementType": "UICN"
                              },
                              "appliesTo":{
                                "country": "GLOBAL"
                              },
                              "authority" : "UICN"
                            }
                          })
                        }
                      }
                      /*
                      SI: (existe "mads_threatStatus ó Authority_nacional")
                      ENTONCES:
                        - Llenar los campos del Plinian-Core.
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.threatCategory.measurementValue = "(Campo mads_threatStatus: ARCHIVO CSV)"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.appliesTo.country = "NACIONAL"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.authority = "MINISTERIO DE AMBIENTE Y DESARROLLO SOSTENIBLE"
                            - ThreatStatusVersion.threatStatus.n.threatStatusAtomized.threatCategory.measurementType = "MADS"
                      */
                      if(authority_nacional!==""){
                        threat_status_version.threatStatus.push({
                          "threatStatusAtomized": {
                            "threatCategory": {
                              "measurementValue": threatCategory_nacional,
                              "measurementType": authority_nacional
                            },
                            "appliesTo":{
                              "country": appliesTo_nacional
                            },
                            "authority" : "MINISTERIO DE AMBIENTE Y DESARROLLO SOSTENIBLE" // Este campo debería venir en el csv
                          }
                        })
                      }

                      if (threat_status_version.threatStatus.length>0){
                        threat_status_version = new ThreatStatusVersion(threat_status_version);
                        console.log("==\n\n====\t\t=====", JSON.stringify(threat_status_version.threatStatus), "\n\n")
                      }else{
                        threat_status_version= undefined
                      }




                      let habitats_version = undefined

                      if (habitatparamo!=="" || habitathumedal!==""){
                        habitats_version = {}
                        habitats_version._id = id_v;
                        habitats_version.created=Date();
                        habitats_version.state="approved_in_use";
                        habitats_version.element="habitats";
                        habitats_version.id_user = 'sib+ac@humboldt.org.co';
                        habitats_version.habitats = {habitatAtomized:[]}

                        var lista = []
                        if (habitatparamo!==""){
                          lista.push({measurementOrFact:{measurementValue: habitatparamo}})
                        }
                        if (habitathumedal!==""){
                          lista.push({measurementOrFact:{measurementValue: habitathumedal}})
                        }
                        habitats_version.habitats.habitatAtomized = lista
                        habitats_version = new HabitatsVersion(habitats_version)

                        console.log("===================\nhabitats_version")
                        console.log(habitats_version.habitats.habitatAtomized)
                      }

                      console.log(' waterfall')
                      console.log('====== waterfall')
                      console.log('============ waterfall')
                      console.log('================== waterfall')
                      console.log('======================== waterfall')
                      console.log('============================== waterfall')


                      async.waterfall([
                        function(callback){
                          if(create_new_record){
                            console.log("se  metio otra vez")
                            var data ={};
                            callback(null, data);
                          }else{
                            RecordVersion.findById(id , function (err, data){
                              console.log("Se metiooooo!!!")
                              if(err){
                                callback(new Error("The Record (Ficha) with id: "+id+" doesn't exist.:" + err.message));
                              }else{
                                console.log("Found a record for the id: "+id);
                                callback(null, data);
                              }
                            });
                          }
                        },
                        // ThreatStatusVersion
                        //////////////////////// Acá Inicia
                        function(data,callback){
                          ThreatStatusVersion.deleteMany({'id_record':id} , function (err, result){
                            if(err){
                              callback(new Error("Error getting list of previous threatStatusVersion:" + err.message));
                            }else{
                              console.log("Deleted list of previous threatStatusVersion", result)
                              callback(null, data);
                            }
                          });
                        },
                        function(data,callback){
                          if(threat_status_version===undefined){
                            callback(null, undefined)
                            return
                          }
                          console.log("Toca crear un nuevo create_new_record ", create_new_record)
                          if(create_new_record){
                            console.log("!");
                            threat_status_version.id_record=id;
                            threat_status_version.version=1;
                            callback(null, threat_status_version);
                          }else{
                            console.log("!*")
                            if(data.threatStatusVersion && data.threatStatusVersion.length !=0){
                              var lenancillaryData = data.threatStatusVersion.length;
                              var idLast = data.threatStatusVersion[lenancillaryData-1];
                              ThreatStatusVersion.findById(idLast , function (err, doc){
                                if(err){
                                  callback(new Error("failed getting the last version of threatStatusVersion:" + err.message));
                                }else{
                                  threat_status_version.id_record=id;
                                  threat_status_version.version=lenancillaryData+1;
                                  callback(null, threat_status_version);
                                }
                              });
                            }else{
                              threat_status_version.id_record=id;
                              threat_status_version.version=1;
                              callback(null, threat_status_version);
                            }
                          }
                        },
                        function(threat_status_version, callback){
                          console.log('Datos recibidos: ', threat_status_version, callback)
                          if(threat_status_version===undefined){
                            callback(null, undefined)
                            return
                          }
                          ver = threat_status_version.version;
                          threat_status_version.save(function(err){
                            if(err){
                              callback(new Error("failed saving the element version:" + err.message));
                            }else{
                              console.log("saved new threat_status_version", threat_status_version);
                              callback(null, threat_status_version);
                            }
                          });
                        },
                        function(threat_status_version, callback){
                          if(threat_status_version===undefined){
                            callback(null, id)
                            return
                          }
                          RecordVersion.findByIdAndUpdate( id, { $push: { "threatStatusVersion": id_v } },{ safe: true, upsert: true }).exec(function (err, record) {
                            if(err){
                              callback(new Error("failed added id to RecordVersion:" + err.message));
                            }else{
                              callback(null, id);
                            }
                          });
                        },

                        ////////////////habitats_version///////////////////


                        function(id, callback){
                          RecordVersion.findById(id , function (err, data){
                            if(err){
                              callback(new Error("The Record (Ficha) with id: "+id+" doesn't exist.:" + err.message));
                            }else{
                              console.log("Found a record for the id: "+id);
                              callback(null, id);
                            }
                          });
                        },


                        function(id,callback){
                          if(habitats_version===undefined){
                            callback(null, undefined)
                            return
                          }
                          console.log("Toca crear un nuevo create_new_record ", create_new_record)
                          console.log("data 681 ", id)
                          if(create_new_record){
                            console.log("!");
                            habitats_version.id_record=id;
                            habitats_version.version=1;
                            callback(null, habitats_version);
                          }else{
                            console.log("!*")
                            if(data.habitatsVersion && data.habitatsVersion.length !=0){
                              var lenhabitatsVersion = data.habitatsVersion.length;
                              var idLast = data.habitatsVersion[lenhabitatsVersion-1];
                              HabitatsVersion.findById(idLast , function (err, doc){
                                if(err){
                                  callback(new Error("failed getting the last version of threatStatusVersion:" + err.message));
                                }else{
                                  habitats_version.id_record=id;
                                  habitats_version.version=lenhabitatsVersion+1;
                                  callback(null, habitats_version);
                                }
                              });
                            }else{
                              console.log("La variable habitats_version es: ", habitats_version)
                              if(habitats_version){
                                habitats_version.id_record=id;
                                habitats_version.version=1;
                                callback(null, habitats_version);
                              }else{
                                callback(null, undefined);
                              }
                            }
                          }
                        },
                        function(habitats_version, callback){
                          if(habitats_version){
                            ver = habitats_version.version;
                            habitats_version.save(function(err){
                              if(err){
                                callback(new Error("failed saving the element version:" + err.message));
                              }else{
                                console.log("saved new habitats_version", habitats_version);
                                callback(null, habitats_version);
                              }
                            });
                          }else{
                            callback(null, undefined);
                          }
                        },
                        function(habitats_version, callback){
                          if(habitats_version){
                            RecordVersion.findByIdAndUpdate( id, { $push: { "habitatsVersion": id_v } },{ safe: true, upsert: true }).exec(function (err, record) {
                              if(err){
                                callback(new Error("failed added id to RecordVersion:" + err.message));
                              }else{
                                callback(null, id);
                              }
                            });
                          }else{
                            callback(null, id);
                          }
                        },


                        function(id, callback){
                          RecordVersion.findById(id , function (err, data){
                            if(err){
                              callback(new Error("The Record (Ficha) with id: "+id+" doesn't exist.:" + err.message));
                            }else{
                              console.log("Found a record for the id: "+id);
                              callback(null, data);
                            }
                          });
                        },
                        //////////////////////// Acá termina


                        function(data,callback){
                          if(create_new_record){
                            console.log("!");
                            ancillary_data_version.id_record=id;
                            ancillary_data_version.version=1;
                            callback(null, ancillary_data_version);
                          }else{
                            console.log("!*")
                            if(data.ancillaryDataVersion && data.ancillaryDataVersion.length !=0){
                              var lenancillaryData = data.ancillaryDataVersion.length;
                              var idLast = data.ancillaryDataVersion[lenancillaryData-1];
                              AncillaryDataVersion.findById(idLast , function (err, doc){
                                if(err){
                                  callback(new Error("failed getting the last version of ancillaryDataVersion:" + err.message));
                                }else{
                                  ancillary_data_version.id_record=id;
                                  ancillary_data_version.version=lenancillaryData+1;
                                  callback(null, ancillary_data_version);
                                }
                              });
                            }else{
                              ancillary_data_version.id_record=id;
                              ancillary_data_version.version=1;
                              callback(null, ancillary_data_version);
                            }
                          }
                        },
                        function(ancillary_data_version, callback){
                          ver = ancillary_data_version.version;
                          ancillary_data_version.save(function(err){
                            if(err){
                              callback(new Error("failed saving the element version:" + err.message));
                            }else{
                              console.log("saved new AncillaryDataVersion");
                              callback(null, ancillary_data_version);
                            }
                          });
                        },
                        function(ancillary_data_version, callback){
                          RecordVersion.findByIdAndUpdate( id, { $push: { "ancillaryDataVersion": id_v } },{ safe: true, upsert: true }).exec(function (err, record) {
                            if(err){
                              callback(new Error("failed added id to RecordVersion:" + err.message));
                            }else{
                              callback(null, id);
                            }
                          });
                        },
                        function(id, callback){
                          console.log("Update Record images");
                          if(create_new_record){
                            console.log("id file------------------------->",id)
                            console.log("threat status version ------------------------->",threat_status_version)
                            console.log("######## UPDATE 809 ############## ", Record_source)
                            Record.update({_id:id},{ threatStatusApprovedInUse: threat_status_version, ancillaryDataApprovedInUse: ancillary_data_version, imageInfo: imageInfo, hierarchy: hierarchyArray, source: Record_source, originAtomized: Record_originAtomized, ident_UICN: ident_UICN, habitats_version: habitats_version }, function(err, result){
                              console.log("Registro almacenado: ", result)
                              if(err){
                                callback(new Error(err.message));
                              }else{
                                number_images_updated++;
                                callback();
                              }
                            });
                          }else{
                            console.log("######## UPDATE 819 ############## ", Record_source, {_id:id, imageInfo:{$exists: false} })
                            console.log("Imagen info ", imageInfo)
                            Record.update({_id:id},{ threatStatusApprovedInUse: threat_status_version, ancillaryDataApprovedInUse: ancillary_data_version, imageInfo: imageInfo, source: Record_source, originAtomized: Record_originAtomized, ident_UICN: ident_UICN, habitats_version: habitats_version }, function(err, result){
                            //Record.update({_id:id, imageInfo:{$exists: false} },{ threatStatusApprovedInUse: threat_status_version, ancillaryDataApprovedInUse: ancillary_data_version, imageInfo: imageInfo, source: Record_source, originAtomized: Record_originAtomized, ident_UICN: ident_UICN, habitats_version: habitats_version }, function(err, result){
                              console.log("Registro almacenado: ", result, err)
                              if(err){
                                callback(new Error(err.message));
                              }else{
                                number_images_updated++;
                                callback();
                              }
                            });
                          }

                        }
                      ],
                      function(err, result) {
                        if (err) {
                          callback(new Error("failed saving AncillaryDataVersion:  " + err.message));
                        }else{
                          console.log('Creation a new AncillaryDataVersion sucess for the record:'+id);
                          callback();
                        }
                      })
                    }
                  },
                  function(callback){
                    console.log("Saved info for: "+canName);
                    callback();
                  }
          			],function (err, result) {
                  if(err){
                      callback(new Error("Error: "+err));
                  }else{
                      callback(null, data);
                  }
					      });
          		},function(err){
            		if(err){
                  		callback(new Error("Error: "+err));
              	}else{
                  		callback(null, data);
              	}
          		});
            console.log("Number: "+number_new_records);
        	}
		], function (err, result) {
    		if(err){
          console.log(err);
        }else{
          console.log("Disconnect the database");
          catalogoDb=mongoose.disconnect();
          console.log("End of the process");
        }
		});
	}
});

