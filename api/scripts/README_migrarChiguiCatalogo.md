El proceso de migración de datos de Chigui al Catálogo de la Biodiversidad se realiza con un script de Bash que se encarga de realizar una serie de tareas relacionadas con la importación de una base de datos MongoDB desde un servidor remoto hacia un servidor local, la ejecución de unos scripts de Node.js y la limpieza de archivos temporales.

Este script se encuentra en el repositorio del catálogo de la biodiversidad en la carpeta api/scripts

<https://gitlab.com/sib-colombia/catalogo-de-la-biodiversidad-backend/-/tree/master/api/scripts/migrarChiguiCatalogo>

En el caso del servidor en donde se encuentra actualmente desplegado está en la carpeta

`/opt/apps/catalogo-de-la-biodiversidad-backend/api/scripts`

Se ejecuta con el comando:

`./migrarChiguiCatalogo`

Este es el shebang que indica que el intérprete a utilizar es Bash.

```bash
#!/bin/bash
```

Aquí se definen las rutas de los directorios donde se van a almacenar los archivos temporales.

```bash
pathOrigen="/home/prodserv"
path="/opt/apps/catalogo-de-la-biodiversidad-backend/api/scripts"
pathBase="$pathOrigen/temp"
pathBaseDestino="$path/temp"
```
Aquí se definen las credenciales y la dirección del servidor remoto donde se encuentra la base de datos MongoDB que se va a importar.

```bash
dbOrigen="chiguiDbXXXXXX" # Solicitar credenciales al SiB Colombia
dbOrigenUser="xxxxx" # Solicitar credenciales al SiB Colombia
dbOrigenPassword="xxxxx"# Solicitar credenciales al SiB Colombia
dbOrigenAutenticationDb="xxxxx" # Solicitar credenciales al SiB Colombia
dbOrigenHost="1.1.1.1:12345" # Solicitar credenciales al SiB Colombia

dbPuenteHost=12345 # Solicitar credenciales al SiB Colombia
dbPuenteLlave="$path/llave.pem" # Solicitar credenciales al SiB Colombia
sshTunelPort=12345 # Solicitar credenciales al SiB Colombia
sshTunelHost="1.1.1.1" # Solicitar credenciales al SiB Colombia
sshTunelUser="usuario" # Solicitar credenciales al SiB Colombia
```

Aquí se definen las credenciales y la dirección del servidor local donde se va a importar la base de datos MongoDB.

```bash
dbDestinoUser="xxxxx" # Solicitar credenciales al SiB Colombia
dbDestinoPassword="xxxxx" # Solicitar credenciales al SiB Colombia
dbDestinoAutenticationDb="xxxxx" # Solicitar credenciales al SiB Colombia
dbDestinoHost="1.1.1.1:12345" # Solicitar credenciales al SiB Colombia
```

Aquí se define la fecha actual para usarla en los nombres de archivo y se definen los nombres de archivo para el backup de la base de datos y el CSV que se va a utilizar en los scripts de Node.js.

```bash
today=$(date +%Y-%m-%d\_%H-%M-%S)
dbOrigenFile="chigui\_$today.bson"
dbDestino="catalogo\_$today"
```

Especificar que es archivo de ejemplo.

```bash
archivoSpecies="CBC\_species\_vXXXXXX.csv"#Archivo csv de especies
```

Aquí se crea el directorio donde se van a almacenar los archivos temporales y se cambia el directorio actual a ese directorio.

```bash
mkdir -p $pathBaseDestino
cd $pathBaseDestino
```

Aquí se crea un túnel SSH al servidor remoto, se crea la carpeta temporal para se ejecuta el comando mongodump para hacer un backup de la base de datos MongoDB en un archivo BSON en el servidor remoto.

```bash
echo "    # Creando puente"
echo "    # Generando backup chigui $dbOrigenFile"
ssh -i $dbPuenteLlave $sshTunelUser@$sshTunelHost -p $sshTunelPort "mkdir -p $pathBase; cd $pathBase; mongodump --host=$dbOrigenHost --archive=$dbOrigenFile --db=$dbOrigen --username $dbOrigenUser --password $dbOrigenPassword --authenticationDatabase $dbOrigenAutenticationDb"
```

Este comando se encarga de crear una conexión SSH con el servidor de origen (\$sshTunelHost) mediante una llave privada (\$dbPuenteLlave) y crear un directorio (\$pathBase) si no existe en el servidor remoto, para posteriormente generar un archivo de backup (\$dbOrigenFile) de la base de datos de origen (\$dbOrigen) usando la herramienta mongodump y los datos de acceso (\$dbOrigenUser, \$dbOrigenPassword, \$dbOrigenAutenticationDb) necesarios para autenticar en la base de datos de origen.

```bash
# Creando puente
# Generando backup chigui $dbOrigenFile
ssh -i $dbPuenteLlave $sshTunelUser@$sshTunelHost -p $sshTunelPort "mkdir -p $pathBase; cd $pathBase; mongodump --host=$dbOrigenHost --archive=$dbOrigenFile --db=$dbOrigen --username $dbOrigenUser --password $dbOrigenPassword --authenticationDatabase $dbOrigenAutenticationDb"
```

Este comando utiliza SFTP (Secure File Transfer Protocol) para copiar el archivo de backup generado anteriormente desde el servidor de origen hacia la máquina local, utilizando la misma llave privada (\$dbPuenteLlave) y la conexión SSH creada en el comando anterior.

```bash
# Copiando entre servidores archivo de backup
sftp -i $dbPuenteLlave -o Port=$sshTunelPort $sshTunelUser@$sshTunelHost:$pathBase/$dbOrigenFile $dbOrigenFile
```

Este comando se encarga de eliminar el archivo de backup (\$dbOrigenFile) y el directorio (\$pathBase) creados anteriormente en el servidor de origen (\$sshTunelHost) utilizando la conexión SSH y la llave privada proporcionadas (\$dbPuenteLlave).

```bash
# Borrando backup chigui \$dbOrigenFile en servidor de origen
ssh -i $dbPuenteLlave $sshTunelUser@$sshTunelHost -p $sshTunelPort "rm -r $pathBase"
```

Este comando utiliza la herramienta mongorestore para importar el archivo de backup generado anteriormente hacia la base de datos de destino (\$dbDestino), utilizando los datos de acceso (\$dbDestinoUser, \$dbDestinoPassword, \$dbDestinoAutenticationDb) necesarios para autenticar en la base de datos de destino. También se especifica el nombre de las colecciones origen y destino (mediante las opciones --nsFrom y --nsTo respectivamente) para realizar la importación.

```bash
# Importando backup en base de datos $dbDestino
mongorestore --host=$dbDestinoHost --username $dbDestinoUser --password $dbDestinoPassword --authenticationDatabase $dbDestinoAutenticationDb --nsFrom "chiguiDbProduccion.\*" --nsTo "$dbDestino.\*"  --archive=$dbOrigenFile
```

Este comando ejecuta un script llamado scriptElementsToRecord.js en la base de datos de destino (\$dbDestino) utilizando Node.js. Este commando es el encargado de migrar los elementos a un documento en la colección Record, que es la base de consulta del Catálogo de la biodiversidad.

```bash
# Ejecutando scriptElementsToRecord ...  $dbDestino
node --max-old-space-size=16000 scriptElementsToRecord.js $dbDestino > scriptElementsToRecord\_$today.out 2> scriptElementsToRecord\_$today.err
```

Después de copiar el archivo de backup desde el servidor de origen al servidor de destino y restaurar la base de datos al servidor de destino, el código ejecuta dos scripts en Node.js para procesar los datos de la base de datos.

```bash
# Ejecutando newRecordsScript ...  $dbDestino"
node --max-old-space-size=16000 newRecordsScript.js $dbDestino $archivoSpecies > newRecordsScript\_$today.out 2> newRecordsScript\_$today.err
```

A continuación, el código limpia los archivos temporales y termina mostrando el nombre de la base de datos creada.

```bash
# Ejecutando newRecordsScript ...  $dbDestino"
node --max-old-space-size=16000 newRecordsScript.js $dbDestino $archivoSpecies > newRecordsScript\_$today.out 2> newRecordsScript\_$today.err
echo "	# ¡Proceso terminado! Base de datos:  $dbDestino"
```

Un ejemplo de ejecución del script:

```bash
prodserv@vps273973:/opt/apps/catalogo-de-la-biodiversidad-backend/api/scripts$ ./migrarChiguiCatalogo
    # Creando puente
    # Generando backup chigui chigui\_2023-05-04\_14-29-23.bson
2023-05-04T20:27:49.589+0200    writing chiguiDbProduccion.UsesManagementAndConservationVersion to archive 'chigui\_2023-05-04\_14-29-23.bson'
2023-05-04T20:27:49.597+0200    writing chiguiDbProduccion.DistributionVersion to archive 'chigui\_2023-05-04\_14-29-23.bson'
…………
2023-05-04T20:27:50.508+0200    done dumping chiguiDbProduccion.SynonymsAtomizedVersion (4683 documents)
2023-05-04T20:27:50.570+0200    done dumping chiguiDbProduccion.EndemicAtomizedVersion (1322 documents)
    # Copiando entre servidores archivo de backup
Connected to 51.38.179.153.
Fetching /home/prodserv/temp/chigui\_2023-05-04\_14-29-23.bson to chigui\_2023-05-04\_14-29-23.bson
/home/prodserv/temp/chigui\_2023-05-04\_14-29-23.bson                                                                                                                                        	100%  478MB  23.3MB/s   00:20    
    # Borrando backup chigui chigui\_2023-05-04\_14-29-23.bson en servidor de origen
    # Importanto backupen base de datos catalogo\_2023-05-04\_14-29-23
2023-05-04T14:29:48.882-0400    preparing collections to restore from
2023-05-04T14:29:48.901-0400    reading metadata for catalogo\_2023-05-04\_14-29-23.AssociatedPartyVersion from archive 'chigui\_2023-05-04\_14-29-23.bson'
2023-05-04T14:29:48.914-0400    restoring catalogo\_2023-05-04\_14-29-23.AssociatedPartyVersion from archive 'chigui\_2023-05-04\_14-29-23.bson'
…………
2023-05-04T14:29:55.758-0400    no indexes to restore
2023-05-04T14:29:55.758-0400    finished restoring catalogo\_2023-05-04\_14-29-23.EndemicAtomizedVersion (538 documents, 0 failures)
2023-05-04T14:29:55.758-0400    169683 document(s) restored successfully. 0 document(s) failed to restore.
    # Ejecutando scriptElementsToRecord ...  catalogo\_2023-05-04\_14-29-23
    # Ejecutando newRecordsScript ...  catalogo\_2023-05-04\_14-29-23
    # Limpiando archivos temporales
    # Proceso terminado! Base de datos:  catalogo\_2023-05-04\_14-29-23
```


Después de ejecutarse el script queda una base de datos con el nombre: `catalogo_2023-05-04_14-29-23`


# Editar archivo catalogo backend

Luego editamos el archivo de configuración del backend para que use la nueva base de datos

```bash
prodserv@vps273973:~$ cd /opt/apps/catalogo-de-la-biodiversidad-backend/
prodserv@vps273973:/opt/apps/catalogo-de-la-biodiversidad-backend$ pico src/config/index.js 
```

Editamos la linea de configuración que define la variable de la base de datos ```DB_NAME```

```javascript
const DB_NAME='catalogo_2023-05-04_14-29-23'
```

Identificamos el proceso que ejecuta el backend
```bash
ps axf | grep /opt/apps/catalogo-de-la-biodiversidad-backend/src/app.js
```

esto nos da una respuesta cómo esta:
```
 172225 pts/1    S+     0:00              \_ grep /opt/apps/catalogo-de-la-biodiversidad-backend/src/app.js
 172195 ?        Sl     0:07      \_ node /opt/apps/catalogo-de-la-biodiversidad-backend/src/app.js
```

Detenemos el proceso que empieza por `node` en este caso el `172195`


```bash
kill -9 172195
```

Esto hace que el servicio se reinicie tomando la nueva configuración. Este número cambia en cada ejecución.
