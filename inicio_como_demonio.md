 * Ingrese a la carpeta del código fuente:

```shell=
cd /opt/apps/catalogo-de-la-biodiversidad-backend
```

 * Ejecute el sigueinte comando para iniciar la aplicación como demonio:
 
 ```shell=
 nohup npm start &
 ```

  * Para matar el proceso busque el id del proceso de catálogo:
```shell=
  ps -fea | grep catalogo
```
  Se deberá mostrar en la consola una salida similar a la siguiente:
  
![](https://docutopia.tupale.co/uploads/upload_767a2bd285e19c1e3dcea4a55242f57a.png)

* Para matar el proceso ejecute el comando: 

```shell=
kill -9 "id_proceso"
```

Para el presente ejemplo, ejecutar los siguientes comandos: 

```shell=
kill -9 12307
kill -9 12323
```

* Tenga en cuanta que es necesario matar el proceso antes de volverlo a iniciar pues se generará un error por intentar correr sobr eun puerto que ya se esta empleando.

